
public class Aufgabe1 {

	public static void main(String[] args) {
		// 
		// Aufgabe 1
				System.out.print("Das ist ein Beispielsatz.");      //print = Satz wird hintereinander fortgef�hrt
				System.out.println(" Ein Beispielplatz ist das.");  //println = neue Zeile f�r darauffolgenden Satz
				
				String bsp1= "\"Beispielsatz\"";
				String bsp2= "Beispielsatz";
				System.out.println("Das ist ein " +  bsp1 +  ".");
				System.out.println("Ein " + bsp2 + " ist das.");
				
	}

}
