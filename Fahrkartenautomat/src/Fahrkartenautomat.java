﻿import java.util.Scanner;

class Fahrkartenautomat {
	static boolean x = true;
	public static void main(String[] args) {
		while ( x== true) {
		
		
		double zuZahlenderBetrag ;
		double eingezahlterGesamtbetrag;
		double rückgabebetrag;
		
		
		
		
		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		
		
		rückgabebetrag = fahrkartenbezahlen(zuZahlenderBetrag);
		
		
		

		// Fahrscheinausgabe
		// -----------------
		
		fahrkartenAusgeben();
		
		

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		
		rueckgeldAusgeben(rückgabebetrag);
	}
		}
	
	
	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		int  anzahlTickets; 
		double ZuZahlenderBetrag;
		double ticketpreis;
		
		
		System.out.print("Zu zahlender Betrag (EURO): ");
		ticketpreis = tastatur.nextDouble();
		System.out.print("Anzahl der Tickets:");
		anzahlTickets =  tastatur.nextInt();
		
		return ticketpreis * anzahlTickets;
		
		}
		
		public static double fahrkartenbezahlen (double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		
		
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			
			
			System.out.printf("Noch zu zahlen:  %.2f EURO \n", zuZahlenderBetrag - eingezahlterGesamtbetrag );
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze; 
		}
		return eingezahlterGesamtbetrag -  zuZahlenderBetrag;
	}
		
		public static void fahrkartenAusgeben() {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
		
}
		public static void rueckgeldAusgeben(double rückgabebetrag) {
			
			if (rückgabebetrag > 0.0) {
				System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
				System.out.println("wird in folgenden Münzen ausgezahlt:");

				while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
				{
					System.out.println("2 EURO");
					rückgabebetrag -= 2.0;
				}
				while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
				{
					System.out.println("1 EURO");
					rückgabebetrag -= 1.0;
				}
				while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
				{
					System.out.println("50 CENT");
					rückgabebetrag -= 0.5;
				}
				while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
				{
					System.out.println("20 CENT");
					rückgabebetrag -= 0.2;
				}
				while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
				{
					System.out.println("10 CENT");
					rückgabebetrag -= 0.1;
				}
				while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
				{
					System.out.println("5 CENT");
					rückgabebetrag -= 0.05;
				}
			}

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");
			
		}
}
