import java.util.Scanner;

class FahrkartenArray
{
    public static void main(String[] args)
    { 
       double zuZahlenderBetrag; 
       double r�ckgabebetrag;

       do {
    	   zuZahlenderBetrag = fahkartenbestellungErfassen();
        	  
    	   r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
   	   
    	   fahrkartenAusgeben();
    	  
    	   rueckgeldAusgeben(r�ckgabebetrag);

    	   System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"+"Wir w�nschen Ihnen eine gute Fahrt.\n");
       }
       	while(true);
 
    }
    
    	public static double fahkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	
    	int wahl;
    	int anzahlTickets;
    	double[] preis = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9};
    	String[] ticketart = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	
    	System.out.println("Auswahlnummer Bezeichnung                             Preis in Euro                 ");
    	for(int i = 0; i < 10; i++) {
    		System.out.printf("%-14d%-40s%-30.2f\n", i+1, ticketart[i], preis[i]);
    	}
    	
    	
    	System.out.print("W�hlen Sie Ihr gew�nschtes Ticket: ");
    	wahl = tastatur.nextInt();
    	while(wahl < 1 || wahl > 10) {
    		System.out.println("  Ihre Eingabe ist ung�ltig!1");
    		System.out.print("Ihre Wahl: ");
    		wahl = tastatur.nextInt();
    	}
    	System.out.printf("%s      %.2f Euro\n", ticketart[wahl-1], preis[wahl-1]);
 
    	System.out.print("Anzahl der Tickets: ");
    	anzahlTickets = tastatur.nextInt();
       
    	if(anzahlTickets < 1 || anzahlTickets > 10) {
        	
        	anzahlTickets = 1;
        	System.out.println("Ung�ltige Ticketanzahl! Ticketanzahl wurde auf 1 gesetzt!");
        }
        
        return anzahlTickets * preis[wahl-1];
    	
    }
    
    	public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneM�nze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	
    	return eingezahlterGesamtbetrag - zuZahlen;

    }
    
    	public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
        	System.out.print("=");
        	
        }
        System.out.println("\n\n");
        
    }
    
    	public static void rueckgeldAusgeben(double rueckgeld) {
    	
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", rueckgeld);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgeld >= 2.0) 
            {
         	 muenzeAusgeben(2, " EURO");
         	 rueckgeld -= 2.0;
            }
            
            while(rueckgeld >= 1.0) 
            {
            	muenzeAusgeben(1, " EURO");
         	 rueckgeld -= 1.0;
            }
            
            while(rueckgeld >= 0.5) 
            {
            	muenzeAusgeben(50, " CENT");
         	 rueckgeld -= 0.5;
            }
            
            while(rueckgeld >= 0.2) 
            {
            	muenzeAusgeben(20, " CENT");
         	 rueckgeld -= 0.2;
            }
            
            while(rueckgeld >= 0.1) 
            {
            	muenzeAusgeben(10, " CENT");
         	 rueckgeld -= 0.1;
            }
            
            while(rueckgeld >= 0.05)
            {
            	muenzeAusgeben(5, " CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    	
    
    	public static void muenzeAusgeben(int betrag, String einheit) {
	   
    		System.out.println(betrag + einheit);
	   
   }

}