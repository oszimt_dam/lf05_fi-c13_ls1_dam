import java.util.Scanner; // Import der Klasse Scanner 
public class Uebung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl1 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl2 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int ergebnis = zahl1 * zahl2;  
	     
	    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
	    System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnis);   
	    
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl3 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl4 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int ergebnis1 = zahl3 + zahl4;  
	     
	    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
	    System.out.println(zahl3 + " + " + zahl4 + " = " + ergebnis1);   
	    
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl5 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine kleinere zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl6 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int ergebnis2 = zahl5 - zahl6;  
	     
	    System.out.print("\n\n\nErgebnis der Substraktion lautet: "); 
	    System.out.print(zahl5 + " - " + zahl6 + " = " + ergebnis2);   
	    
	    
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl7 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine kleine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl8 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int ergebnis4 = zahl7 / zahl8;  
	     
	    System.out.print("\n\n\nErgebnis der Division lautet: "); 
	    System.out.println(zahl7 + " / " + zahl8 + " = " + ergebnis4);  
	    
	    myScanner.close(); 
	}

}
