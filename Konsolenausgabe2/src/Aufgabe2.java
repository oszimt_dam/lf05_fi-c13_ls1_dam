
public class Aufgabe2 {

	public static void main(String[] args) {
		double a = -28.8889;
		double b = -23.3333;
		double c = -17.7778;
		double d = -6.6667;
		double e = -1.1111;

		
		System.out.println("Fahrenheit  |  Celsius");
		System.out.println("----------------------");
		System.out.printf("    -20     | %.2f\n", a);
		System.out.printf("    -10     | %.2f\n", b);
		System.out.printf("      0     | %.2f\n", c);
		System.out.printf("     20     | %.2f\n", d);
		System.out.printf("     30     | %.2f\n", e);
	}

}
