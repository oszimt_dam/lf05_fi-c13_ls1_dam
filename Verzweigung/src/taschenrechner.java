import java.util.Scanner;


public class taschenrechner {
  
  public static void main(String[] args) {
    
    char operator = ' ';
    double zahl1;
    double zahl2;
    double ergebnis = 0.0;
    
    Scanner tastatur = new Scanner(System.in);
    
    
    System.out.println("Geben Sie die erste Zahl ein: ");
    zahl1 = tastatur.nextDouble();
    System.out.println("Geben Sie die zweite Zahl ein: ");
    zahl2 = tastatur.nextDouble();
       
    System.out.println("Bitte geben Sie eines der folgenden Zeichen ein:  Addition +, Subtraktion -, Multiplikation *, Division /");
 
    operator = tastatur.next().charAt(0);
    
    if (operator == '+') {
      ergebnis = zahl1 + zahl2;
      System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(ergebnis * 100)/100.0 );
    } 
    else if (operator == '-') {
        ergebnis = zahl1 - zahl2;
        System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(ergebnis * 100)/100.0 );
    }
    else if (operator == '*') {
          ergebnis = zahl1 * zahl2;
          System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(ergebnis * 100)/100.0 );
    }
    else if (operator == '/') {
             ergebnis = zahl1 / zahl2;
            System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(ergebnis * 100)/100.0 );
    }
    else {
      System.out.println("Sie haben keinen g�ltigen Operator eingegeben!");
    } 
    
    
    
    
  }

} 
